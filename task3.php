<?php
/**
 * task3
 * Date: 10/29/2016
 * Time: 10:34 AM
 */

    class MyCalculator
    {
        public $number1;
        public $number2;



        public function __construct($num1,$num2)
        {
            $this->number1=$num1;
            $this->number2=$num2;
        }

        public function add(){
            $sum = $this->number1 + $this->number2;
            return $sum;
        }

        public function multiply(){
            $multiple = $this->number1 * $this->number2;
            return $multiple;

        }

        public function subtract(){
            $sub= $this->number1 +$this->number2;
            return $sub;
        }

        public function divide(){
            if($this->number2 == 0){
                echo "You can't divide by Zero!";
            }
            $div = $this->number1 / $this->number2;
            return $div;
        }



    }

    $mycalc= new MyCalculator(12,6);

    echo "Sum of".$mycalc->add()."<br>";
    echo "Multiplying result is ".$mycalc->multiply()."<br>";
    echo "Subtract is ".$mycalc->subtract()."<br>";
    echo "Division is ".$mycalc->divide();