<?php
/**
 * task2
 * Date: 10/29/2016
 * Time: 10:26 AM
 */

    $startDate = new DateTime("1981-11-03-12:45");
    $endDate = new DateTime("2015-10-29-02:20");

    $difference = $startDate-> diff($endDate);

    echo "Difference : " . $difference->y . " years " . $difference->m." months ". $difference->d." day ".$difference->h." hour ".$difference->m." minute ".$difference->s." seconds ";
