<?php
/**
 * Task 1
 * Date: 10/29/2016
 * Time: 10:09 AM
 */
class array_sort
{
    protected $_asort;

    public function __construct(array $numbers)
    {
        $this->_asort = $numbers;
    }

    public function mySort()
    {
        $sortingValue = $this->_asort;
        sort($sortingValue);
        return $sortingValue;
    }
}
    $sortarray =  new array_sort(array(11,-2,4,35,0,8,-9));

    print_r($sortarray->mySort());


