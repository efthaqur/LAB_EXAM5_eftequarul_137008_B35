<?php
/**
 *
 * task4
 * Date: 10/29/2016
 * Time: 10:35 AM
 */
    class factorial_of_a_number
    {

        protected $_number;

        public function __construct($number)
        {
            if(!is_int($number))
            {
                throw new InvalidArgumentException('Not a number or Missing arguemnt');
            }
           $this->_number=$number;
        }

        public function result()
        {
            $factorial = 1;
            for ($i=1; $i<=$this->_number;$i++)
            {
                $factorial*=$i;
            }
            return $factorial;
        }


    }

    $newfactorial= new factorial_of_a_number(5);

    echo "Factorial is = ".$newfactorial->result();